#
#  Be sure to run `pod spec lint MMSmartStreamingSDK.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see https://guides.cocoapods.org/syntax/podspec.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |spec|

  spec.name         = "TestVOSDK"
  spec.version      = "0.1.7"
  spec.summary      = "A short description of MMSmartStreamingSDK."
  spec.description  = 'The MediaMelon Player SDK adds SmartSight Analytics and QBR SmartStreaming capability to any media player and is available for all ABR media players.'

  spec.homepage     = "https://bitbucket.org/anurag610/votestpod.git"

  spec.license      = { :type => 'MIT', :file => 'LICENSE' }

  spec.author       = { "Anurag" => "singhanurag1234@gmail.com" }

  spec.platform     = :ios, "9.0"

  #  When using multiple platforms
  spec.ios.deployment_target = "9.0"

  spec.source       = { :git => "https://bitbucket.org/anurag610/votestpod.git", :tag => "#{spec.version}" }

  spec.source_files = 'TestVOSDK/Classes/Common/*.{h,m,swift}', 'TestVOSDK/Classes/Wrapper/ssai/*.swift', 'TestVOSDK/Classes/Utilities/M3U8Parser/*.{h,m}', 'TestVOSDK/Classes/Utilities/VastClient/*.{h,swift}'
  spec.ios.frameworks = 'CoreTelephony'
  spec.ios.vendored_libraries = 'TestVOSDK/Classes/StaticLibrary/iOS/libmmsmartstreamer.a'
  spec.libraries = 'stdc++'
  spec.public_header_files = 'TestVOSDK/Classes/Common/*.h', 'TestVOSDK/Classes/Utilities/M3U8Parser/*.h'
  spec.swift_version    = '4.0'
  spec.pod_target_xcconfig = {'VALID_ARCHS[sdk=iphonesimulator*]' => 'armv7 arm64 x86_64'}

 

end
