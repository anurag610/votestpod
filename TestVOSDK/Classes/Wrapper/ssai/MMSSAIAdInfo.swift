//
//  MMSSAIAdInfo.swift
//  VOPlayerDemoSwift
//
//  Created by Anurag Singh on 4/2/21.
//  Copyright © 2021 VOPlayer. All rights reserved.
//

import Foundation

public class MMSSAIAdInfo {
    
    public var adId: String = ""
    public var adTitle: String = ""
    public var adServer: String = ""
    public var adIndex: Int = 0
    public var adDuration: u_long = 0
    public var startTime: u_long = 0
    public var endTime: u_long = 0
    public var firstQuartile: u_long = 0
    public var midPoint: u_long = 0
    public var thirdQuartile: u_long = 0
    public var complete: u_long = 0
    public var adCurrentPlaybackTimeInSec: u_long = 0
    public var position: String = ""
    public var active: Bool = false
    public var adState: String = ""
    public var streamType: String = ""
    public var isLinear: Bool = false
    public var adTrackerInfo: AdTrackerInfo?
    public var adImpressionsTemplates: [VastImpression]=[]
    public var adTrackingEvents: [VastTrackingEvent]=[]
    
    public init(aId: String, aTitle: String, aServer: String, aIndex: Int, aDuration: u_long, sTime: u_long, eTime: u_long, fqTime: u_long, mpTime: u_long, tqTime: u_long, complete: u_long, adCurrentTime: u_long, position: String, active: Bool, aState: String, sType: String, isLinear: Bool, atInfo: AdTrackerInfo, aImpressionsTemplates:[VastImpression], aTrackingEvents: [VastTrackingEvent]) {
        self.adId = aId
        self.adTitle = aTitle
        self.adServer = aServer
        self.adIndex = aIndex
        self.adDuration = aDuration
        self.startTime = sTime
        self.endTime = eTime
        self.firstQuartile = fqTime
        self.midPoint = mpTime
        self.thirdQuartile = tqTime
        self.complete = complete
        self.adCurrentPlaybackTimeInSec = adCurrentTime
        self.position = position
        self.active = active
        self.adState = aState
        self.streamType = sType
        self.isLinear = isLinear
        self.adTrackerInfo = atInfo
        self.adImpressionsTemplates = aImpressionsTemplates
        self.adTrackingEvents = aTrackingEvents
    }
    
    public func updateAdState(state: String) {
        self.adState = state
    }
}

public class AdTrackerInfo {
    
    public var isAdImpressionSent: Bool = false
    public var isAdStartSent: Bool = false
    public var isFirstQuartileSent = false
    public var isMidPointSent = false
    public var isThirdQuartileSent = false
    public var isAdCompleteSent = false
    
    public init() {
        
    }
    
    public func resetToDefault() {
        isAdImpressionSent = false
        isAdStartSent = false
        isFirstQuartileSent = false
        isMidPointSent = false
        isThirdQuartileSent = false
        isAdCompleteSent = false
    }
    
    public func setAdComplete() {
        isAdImpressionSent = false;
        isAdStartSent = false;
        isFirstQuartileSent = false;
        isMidPointSent = false;
        isThirdQuartileSent = false;
        isAdCompleteSent = true;
    }
}
