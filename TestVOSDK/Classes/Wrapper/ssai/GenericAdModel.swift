//
//  GenericAdModel.swift
//  VOPlayerDemoSwift
//
//  Created by Anurag Singh on 6/10/21.
//  Copyright © 2021 VOPlayer. All rights reserved.
//

import Foundation
import TestVOSDK

public enum MMVOAdSate: Int {
    case Request = 0
    case AdPlaying
    case AdPaused
    case AdSkipped
    case AdCompleted
    case AdError
    case AdBlocked
    case AdImpression
    case AdStarted
    case AdClicked
    case AdResumed
    case AdFirstQuartile
    case AdMidpoint
    case AdThirdQuartile
    case AdEnded
}

public class MMVOSSAIAdInfo {
    public var adId: String = ""
    public var adTitle: String = ""
    public var adServer: String = ""
    public var adIndex: Int = 0
    public var adDuration: u_long = 0
    public var startTime: u_long = 0
    public var endTime: u_long = 0
    public var firstQuartile: u_long = 0
    public var midPoint: u_long = 0
    public var thirdQuartile: u_long = 0
    public var complete: u_long = 0
    public var adCurrentPlaybackTimeInSec: u_long = 0
    public var position: String = ""
    public var active: Bool = false
    public var adState: String = ""
    public var streamType: String = ""
    public var isLinear: Bool = false
    var adTrackerInfo: AdTrackerInfo?
    public var adImpressionsTemplates: [VastImpression]=[]
    public var adTrackingEvents: [VastTrackingEvent]=[]

    init(aId: String, aTitle: String, aServer: String, aIndex: Int, aDuration: u_long, sTime: u_long, eTime: u_long, fqTime: u_long, mpTime: u_long, tqTime: u_long, complete: u_long, adCurrentTime: u_long, position: String, active: Bool, aState: String, sType: String, isLinear: Bool, atInfo: AdTrackerInfo, aImpressionsTemplates:[VastImpression], aTrackingEvents: [VastTrackingEvent]) {
        self.adId = aId
        self.adTitle = aTitle
        self.adServer = aServer
        self.adIndex = aIndex
        self.adDuration = aDuration
        self.startTime = sTime
        self.endTime = eTime
        self.firstQuartile = fqTime
        self.midPoint = mpTime
        self.thirdQuartile = tqTime
        self.complete = complete
        self.adCurrentPlaybackTimeInSec = adCurrentTime
        self.position = position
        self.active = active
        self.adState = aState
        self.streamType = sType
        self.isLinear = isLinear
        self.adTrackerInfo = atInfo
        self.adImpressionsTemplates = aImpressionsTemplates
        self.adTrackingEvents = aTrackingEvents
    }
}

public protocol GenericAdProtocol {
    func notifyVOAdWith(eventName: MMVOAdSate, adInfo: MMVOSSAIAdInfo)
}

protocol SSAIManagerDelegate {
    func notifyAdEventsWith(eventName: MMAdState, andAdInfo adInfo: MMAdInfo)
    func notifyMMSSAIAdEventsWith(eventName: MMAdState, andAdInfo adInfo: MMSSAIAdInfo)
}

public enum MMCurrentPlayerState {
    case IDLE,
    PLAYING,
    PAUSED,
    STOPPED,
    ERROR
};
