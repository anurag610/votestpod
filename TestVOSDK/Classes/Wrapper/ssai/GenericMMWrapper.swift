
//
//  GenericMMWrapper.swift
//  GenericPods-MMSmartStreaming-iOS
//
//  Created by Anurag Singh on 6/9/21.
//  Copyright © 2021 MediaMelon. All rights reserved.
//

import Foundation
import UIKit
import AVKit
import AVFoundation
import CoreTelephony

@objc public class GenericMMWrapper: NSObject, MMSmartStreamingObserver {

    private var lastReportedBitrate = 0
    fileprivate let TIME_INCREMENT = 2.0
    fileprivate var currentAdState = MMAdState.UNKNOWN
    private var assetInfo: MMAssetInformation?
    private static var enableLogging = true
    lazy var mmSmartStreaming:MMSmartStreaming = MMSmartStreaming.getInstance() as! MMSmartStreaming
    private var isAdStreaming = false
    private var isAdBuffering = false
    private var isPostRollAd = false
    private var adDuration = 0
    private var mmssaiDelegate: SSAIManagerDelegate?
    public static let shared: GenericMMWrapper = GenericMMWrapper()
    var adManager: MMSSAIAdManager?
    private var url: String = ""
    var vastUrl: String = ""
    var isLive: Bool = false
    public var voAdDelegate: GenericAdProtocol?
    
    
    //MARK:- MMSDK INITIALIZATION DELEGATE
    @objc public func sessionInitializationCompleted(with status: MMSmartStreamingInitializationStatus, andDescription description: String!, forCmdWithId cmdId: Int) {
        GenericMMWrapper.logDebugStatement("sessionInitializationCompleted - status \(status) description \(String(describing: description))")
    }
    
    public func initialiseSSAIAdManager(mediaUrl: String, vastUrl: String, isLive: Bool) {
        self.isLive = isLive
        self.adManager?.setupSSAIAdManager(mediaURL: mediaUrl, vastUrl: vastUrl, isLive: isLive)
    }
    
    @objc public static func getVersion() -> String {
        return "4.0.0/\(String(describing: MMSmartStreaming.getVersion()!))"
    }
    
    @objc public static func disableManifestsFetch(disable: Bool) {
        return MMSmartStreaming.disableManifestsFetch(disable)
    }
    
    private func initSession(deep: Bool) {
        GenericMMWrapper.logDebugStatement("*** initSession")
        guard let assetInfo = self.assetInfo else {
            GenericMMWrapper.logDebugStatement("!!! Error - assetInfo not set !!!")
            return
        }

        GenericMMWrapper.shared.initializeSession(mode: assetInfo.qbrMode, manifestURL: assetInfo.assetURL, metaURL: assetInfo.metafileURL?.absoluteString, assetInfo: assetInfo)
        for (key, value) in assetInfo.customKVPs {
            GenericMMWrapper.shared.reportCustomMetadata(key: key, value: value)
        }
        let url = assetInfo.assetURL
        GenericMMWrapper.logDebugStatement("Initializing for \(String(describing: url))")
    }
    
    @objc public static func cleanUp() {
        GenericMMWrapper.logDebugStatement("================cleanUp=============")
    }
    
    /**
     * Application may update the subscriber information once it is set via MMRegistrationInformation
     */
    public static func updateSubscriber(subscriberId: String!, subscriberType: String!, subscriberMetadata: String!) {
        MMSmartStreaming.updateSubscriber(withID: subscriberId, andType: subscriberType, withTag:subscriberMetadata)
    }
    
    /**
     * Application may report the custom metadata associated with the content using this API.
     * Application can set it as a part of MMAVAssetInformation before the start of playback, and
     * can use this API to set metadata during the course of the playback.
     */
    public func reportCustomMetadata(key: String!, value: String!) {
        self.mmSmartStreaming.reportCustomMetadata(withKey: key, andValue: value)
    }
    
    /**
     * Used for debugging purposes, to enable the log trace
     */
    public func enableLogTrace(logStTrace: Bool) {
        GenericMMWrapper.enableLogging = logStTrace
        self.mmSmartStreaming.enableLogTrace(logStTrace)
    }
    
    /**
     * If application wants to send application specific error information to SDK, the application can use this API.
     * Note - SDK internally takes care of notifying the error messages provided to it by AVFoundation framwork
     */
    public func reportError(error: String, playbackPosMilliSec: Int) {
        self.mmSmartStreaming.reportError(error, atPosition: playbackPosMilliSec)
    }
    
    public static func reportMetricValue(metricToOverride: MMOverridableMetric, value: String!) {
        GenericMMWrapper.shared.mmSmartStreaming.reportMetricValue(for: MMOverridableMetric.ServerAddress, value: value)
    }
    
    private static func logDebugStatement(_ logStatement: String) {
        if(GenericMMWrapper.enableLogging) {
            print("MM_Log:- \(logStatement)")
        }
    }
    
    public func registerForMMSDK(registrationInformation pInfo: MMRegistrationInformation) {
        GenericMMWrapper.logDebugStatement("=============setPlayerInformation - pInfo=============")
        GenericMMWrapper.registerMMSmartStreaming(playerName: pInfo.playerName, custID: pInfo.customerID, component: pInfo.component, subscriberID: pInfo.subscriberID, domainName: pInfo.domainName, subscriberType: pInfo.subscriberType, subscriberTag: pInfo.subscriberTag)
        GenericMMWrapper.reportPlayerInfo(brand: pInfo.playerBrand, model: pInfo.playerModel, version: pInfo.playerVersion)
    }
    
    private static func registerMMSmartStreaming(playerName: String, custID: String, component: String,  subscriberID: String?, domainName: String?, subscriberType: String?, subscriberTag: String?) {
        
        MMSmartStreaming.registerForPlayer(withName: playerName, forCustID: custID, component: component, subsID: subscriberID, domainName: domainName, andSubscriberType: subscriberType, withTag:subscriberTag);
        
        var operatorName = ""
        var osName = ""
        let osVersion = UIDevice.current.systemVersion
        let brand = "Apple"
        let model = UIDevice.current.model
        let screenWidth = Int(UIScreen.main.bounds.width)
        let screenHeight = Int(UIScreen.main.bounds.height)
        
        let phoneInfo = CTTelephonyNetworkInfo()
        if #available(iOS 12.0, *) {
            if let carrier = phoneInfo.serviceSubscriberCellularProviders, let dict = carrier.first, let opName = dict.value.carrierName {
                operatorName = opName
            }
        }
        else {
            if let carrierName = phoneInfo.subscriberCellularProvider?.carrierName{
                operatorName = carrierName
            }
        }
        osName = "iOS"
        MMSmartStreaming.reportDeviceInfo(withBrandName: brand, deviceModel: model, osName: osName, osVersion: osVersion, telOperator: operatorName, screenWidth: screenWidth, screenHeight: screenHeight, andType: model)
    }
    
    private static func reportPlayerInfo(brand: String?, model: String?, version: String?) {
        MMSmartStreaming.reportPlayerInfo(withBrandName: brand, model: model, andVersion: version)
    }
    
    public func initialiseSession(assetInformation: MMAssetInformation) {
        GenericMMWrapper.shared.initializeSession(mode: assetInformation.qbrMode, manifestURL: assetInformation.assetURL, metaURL: assetInformation.metafileURL?.absoluteString, assetInfo: assetInformation)
    }
    
    private func initializeSession(mode: MMQBRMode!, manifestURL: String!, metaURL: String?, assetID: String?, assetName: String?, videoId: String?) {
        var connectionInfo: MMConnectionInfo!
        let reachability = ReachabilityMM()

        func getDetailedMobileNetworkType() {
            let phoneInfo = CTTelephonyNetworkInfo()
            if #available(iOS 12.0, *) {
                if let arrayNetworks = phoneInfo.serviceCurrentRadioAccessTechnology {
                    var networkString = ""
                    for value in arrayNetworks.values {
                        networkString = value
                    }
                    if networkString == CTRadioAccessTechnologyLTE{
                        connectionInfo = .cellular_4G
                    }else if networkString == CTRadioAccessTechnologyWCDMA{
                        connectionInfo = .cellular_3G
                    }else if networkString == CTRadioAccessTechnologyEdge{
                        connectionInfo = .cellular_2G
                    }
                }
            } else {
                let networkString = phoneInfo.currentRadioAccessTechnology
                if networkString == CTRadioAccessTechnologyLTE{
                    connectionInfo = .cellular_4G
                }else if networkString == CTRadioAccessTechnologyWCDMA{
                    connectionInfo = .cellular_3G
                }else if networkString == CTRadioAccessTechnologyEdge{
                    connectionInfo = .cellular_2G
                }
            }
        }

        if let reachability = reachability{
            do{
                try reachability.startNotifier()
                let status = reachability.currentReachabilityStatus

                if(status == .notReachable){
                    connectionInfo = .notReachable
                }
                else if (status == .reachableViaWiFi){
                    connectionInfo = .wifi
                }
                else if (status == .reachableViaWWAN){
                    connectionInfo = .cellular
                    getDetailedMobileNetworkType()
                }
            }
            catch{

            }
        }
        if (connectionInfo != nil) {
            self.mmSmartStreaming.reportNetworkType(connectionInfo)
        }
        mmSmartStreaming.initializeSession(with: mode, forManifest: manifestURL, metaURL: metaURL, assetID: assetID, assetName: assetName, videoID:videoId, for: self)
    }
    
    private func initializeSession(mode: MMQBRMode!, manifestURL: String!, metaURL: String?, assetInfo: MMAssetInformation) {
        var connectionInfo: MMConnectionInfo!
        let reachability = ReachabilityMM()
        
        func getDetailedMobileNetworkType() {
            let phoneInfo = CTTelephonyNetworkInfo()
            if #available(iOS 12.0, *) {
                if let arrayNetworks = phoneInfo.serviceCurrentRadioAccessTechnology {
                    var networkString = ""
                    for value in arrayNetworks.values {
                        networkString = value
                    }
                    if networkString == CTRadioAccessTechnologyLTE{
                        connectionInfo = .cellular_4G
                    }else if networkString == CTRadioAccessTechnologyWCDMA{
                        connectionInfo = .cellular_3G
                    }else if networkString == CTRadioAccessTechnologyEdge{
                        connectionInfo = .cellular_2G
                    }
                }
            } else {
                let networkString = phoneInfo.currentRadioAccessTechnology
                if networkString == CTRadioAccessTechnologyLTE{
                    connectionInfo = .cellular_4G
                }else if networkString == CTRadioAccessTechnologyWCDMA{
                    connectionInfo = .cellular_3G
                }else if networkString == CTRadioAccessTechnologyEdge{
                    connectionInfo = .cellular_2G
                }
            }
        }
        
        if let reachability = reachability{
            do{
                try reachability.startNotifier()
                let status = reachability.currentReachabilityStatus
                
                if(status == .notReachable){
                    connectionInfo = .notReachable
                }
                else if (status == .reachableViaWiFi){
                    connectionInfo = .wifi
                }
                else if (status == .reachableViaWWAN){
                    connectionInfo = .cellular
                    getDetailedMobileNetworkType()
                }
            }
            catch{
                
            }
        }
        if (connectionInfo != nil) {
            self.mmSmartStreaming.reportNetworkType(connectionInfo)
        }
        let contentMetadata = MMContentMetadata()
        contentMetadata.assetId = assetInfo.assetID
        contentMetadata.assetName = assetInfo.assetName
        contentMetadata.videoId = assetInfo.videoId
        contentMetadata.contentType = assetInfo.contentType
        contentMetadata.genre = assetInfo.genre
        contentMetadata.season = assetInfo.season
        contentMetadata.drmProtection = assetInfo.drmProtection
        contentMetadata.seriesTitle = assetInfo.seriesTitle
        contentMetadata.episodeNumber = assetInfo.episodeNumber
        
        url = manifestURL
        mmSmartStreaming.initializeSession(with: mode, forManifest: manifestURL, metaURL: metaURL, contentMetadata: contentMetadata, for: self)
        if adManager == nil {
            adManager = MMSSAIAdManager()
            adManager?.delegate = self
        }
        mmSmartStreaming.reportUserInitiatedPlayback()
    }
    
    public func reportBufferingStart() {
        GenericMMWrapper.shared.reportBufferingStarted()
    }
    
    private func reportBufferingStarted() {
        self.mmSmartStreaming.reportBufferingStarted()
    }
    
    public func reportBufferingComplete() {
        GenericMMWrapper.shared.reportBufferingCompleted()
    }
    
    private func reportBufferingCompleted() {
        self.mmSmartStreaming.reportBufferingCompleted()
    }
    
    public func reportPlayerState(playerState: MMPlayerState) {
        self.mmSmartStreaming.report(playerState)
    }
    
    public func updatePlaybackPosition(currentPosition: Int) {
        self.updatePlayerPlaybackPosition(currentPosition: currentPosition)
    }
    
    private func updatePlayerPlaybackPosition(currentPosition: Int){
        var currentPlayPosInMs = currentPosition
        if(currentPlayPosInMs >= 0){
            currentPlayPosInMs = currentPlayPosInMs * 1000
            mmSmartStreaming.reportPlaybackPosition(currentPlayPosInMs)
            self.adManager?.notifyPlayerPlaybackTime(playbackTime: currentPlayPosInMs)
        }
    }
    
    public func updatePresentationInformation(duration: Int, presentationInfos: [VOPlayerAlternateInfo]) {
        let presentationInfo = MMPresentationInfo()
        presentationInfo.duration = duration
            for alternateInfo in presentationInfos {
                if alternateInfo.bitrate != nil && alternateInfo.bitrate! > 0 {
                    let rePresentation = MMRepresentation()
                    rePresentation.trackIdx = alternateInfo.trackIndex ?? 0
                    rePresentation.bitrate = alternateInfo.bitrate ?? 0
                    rePresentation.width = alternateInfo.width ?? 0
                    rePresentation.height = alternateInfo.height ?? 0
                    rePresentation.codecIdentifier = nil
                    presentationInfo.representations.append(rePresentation)
                }
        }
        mmSmartStreaming.setPresentationInformation(presentationInfo)
    }
    
    public func reportChunkRequest(bitrate: Int, fragmentDuration: Int?, url: String?) {
        let chunkInformation = MMChunkInformation()
        chunkInformation.bitrate = bitrate
        chunkInformation.duration = fragmentDuration ?? -1
        chunkInformation.resourceURL = url
        if bitrate > 0 {
            mmSmartStreaming.reportChunkRequest(chunkInformation)
        }
    }
    
    public func reportDownloadRate(downloadRate: Int) {
        mmSmartStreaming.reportDownloadRate(downloadRate)
    }

    public func seekPositionChanged(currentPlaybackTime: Int) {
        mmSmartStreaming.reportPlayerSeekCompleted(currentPlaybackTime)
    }
    
    
    fileprivate func fetchAndReportAdInfo(forAd ad: VastAd? = nil, adTracker: VastTracker? = nil, adIndex: Int, ssaiAdInfo: MMSSAIAdInfo? = nil) -> MMAdInfo {
        let advertisement = ad
        
        let adInfo = MMAdInfo()
        adInfo.adClient = "Nowtilus"
        adInfo.adId = advertisement?.id ?? ssaiAdInfo?.adId ?? ""
        
        if advertisement != nil {
            switch advertisement!.type {
            case .inline:
                adInfo.adType = MMAdType.AD_LINEAR
            default:
                adInfo.adType = MMAdType.AD_UNKNOWN
            }
            
            if let adCreative = advertisement!.creatives.first {
                if let linear = adCreative.linear {
                    adInfo.adType = MMAdType.AD_LINEAR
                    if let duration = linear.duration {
                        adInfo.adDuration = Int(duration * 1000)
                    }
                }
            }
        } else {
            adInfo.adType = .AD_LINEAR
        }
        adInfo.adPosition = "mid"
        adInfo.adPodIndex = 0
        adInfo.adPositionInPod = adIndex
        self.mmSmartStreaming.report(adInfo)
        return adInfo
    }
    
    
}

//MARK:- MMSSAI ADMANAGER DELEGATE
extension GenericMMWrapper: MMSSAIDelegate {
    
    public func notifySSAIAdEventWith(state: MMAdState, adInfo: MMSSAIAdInfo) {
        self.mmssaiDelegate?.notifyMMSSAIAdEventsWith(eventName: state, andAdInfo: adInfo)
        
        let voAdInfo = MMVOSSAIAdInfo(aId: adInfo.adId, aTitle: adInfo.adTitle, aServer: adInfo.adServer, aIndex: adInfo.adIndex, aDuration: adInfo.adDuration, sTime: adInfo.startTime, eTime: adInfo.endTime, fqTime: adInfo.firstQuartile, mpTime: adInfo.midPoint, tqTime: adInfo.thirdQuartile, complete: adInfo.complete, adCurrentTime: adInfo.adCurrentPlaybackTimeInSec, position: adInfo.position, active: adInfo.active, aState: adInfo.adState, sType: adInfo.streamType, isLinear: adInfo.isLinear, atInfo: AdTrackerInfo(), aImpressionsTemplates: adInfo.adImpressionsTemplates, aTrackingEvents: adInfo.adTrackingEvents)
        
        switch state {
        case .AD_REQUEST:
            self.mmSmartStreaming.report(MMAdState.AD_REQUEST)
            voAdDelegate?.notifyVOAdWith(eventName: .Request, adInfo: voAdInfo)

        case .AD_IMPRESSION:
            self.isAdBuffering = false
            self.isAdStreaming = true
            self.mmSmartStreaming.reportAdPlaybackTime(0)
            self.mmSmartStreaming.report(MMAdState.AD_IMPRESSION)
            self.currentAdState = MMAdState.AD_IMPRESSION
            voAdDelegate?.notifyVOAdWith(eventName: .AdImpression, adInfo: voAdInfo)

        case .AD_STARTED:
            self.isAdBuffering = false
            self.isAdStreaming = true
            self.mmSmartStreaming.reportAdPlaybackTime(0)
            if (isAdBuffering) {
                self.isAdBuffering = false
            }
            self.mmSmartStreaming.report(MMAdState.AD_STARTED)
            self.currentAdState = MMAdState.AD_STARTED
            voAdDelegate?.notifyVOAdWith(eventName: .AdStarted, adInfo: voAdInfo)

        case .AD_CLICKED:
            self.isAdStreaming = false
            self.mmSmartStreaming.report(MMAdState.AD_CLICKED)
            self.currentAdState = MMAdState.AD_CLICKED
            voAdDelegate?.notifyVOAdWith(eventName: .AdClicked, adInfo: voAdInfo)

        case .AD_COMPLETED:
            self.isAdStreaming = false
            self.mmSmartStreaming.reportAdPlaybackTime(self.adDuration * 1000)
            self.mmSmartStreaming.report(MMAdState.AD_COMPLETED)
            self.currentAdState = MMAdState.AD_COMPLETED
            voAdDelegate?.notifyVOAdWith(eventName: .AdCompleted, adInfo: voAdInfo)

        case .AD_ENDED:
            self.isAdStreaming = false
            self.mmSmartStreaming.report(MMAdState.AD_COMPLETED)
            self.currentAdState = MMAdState.AD_ENDED
            voAdDelegate?.notifyVOAdWith(eventName: .AdEnded, adInfo: voAdInfo)

        case .AD_PAUSED:
            self.isAdStreaming = true
            self.mmSmartStreaming.report(MMAdState.AD_PAUSED)
            self.currentAdState = MMAdState.AD_PAUSED
            voAdDelegate?.notifyVOAdWith(eventName: .AdPaused, adInfo: voAdInfo)

        case .AD_RESUMED:
            self.isAdStreaming = true
            self.mmSmartStreaming.report(MMAdState.AD_RESUMED)
            self.currentAdState = MMAdState.AD_RESUMED
            voAdDelegate?.notifyVOAdWith(eventName: .AdResumed, adInfo: voAdInfo)

        case .AD_SKIPPED:
            self.isAdStreaming = false
            self.mmSmartStreaming.report(MMAdState.AD_SKIPPED)
            self.currentAdState = MMAdState.AD_SKIPPED
            voAdDelegate?.notifyVOAdWith(eventName: .AdSkipped, adInfo: voAdInfo)

        case .AD_FIRST_QUARTILE:
            self.isAdStreaming = true
            self.mmSmartStreaming.report(MMAdState.AD_FIRST_QUARTILE)
            self.currentAdState = MMAdState.AD_FIRST_QUARTILE
            voAdDelegate?.notifyVOAdWith(eventName: .AdFirstQuartile, adInfo: voAdInfo)

        case .AD_MIDPOINT:
            self.isAdStreaming = true
            self.mmSmartStreaming.report(MMAdState.AD_MIDPOINT)
            self.currentAdState = MMAdState.AD_MIDPOINT
            voAdDelegate?.notifyVOAdWith(eventName: .AdMidpoint, adInfo: voAdInfo)

        case .AD_THIRD_QUARTILE:
            self.isAdStreaming = true
            self.mmSmartStreaming.report(MMAdState.AD_THIRD_QUARTILE)
            self.currentAdState = MMAdState.AD_THIRD_QUARTILE
            voAdDelegate?.notifyVOAdWith(eventName: .AdThirdQuartile, adInfo: voAdInfo)

        case .AD_ERROR:
            self.mmSmartStreaming.report(MMAdState.AD_ERROR)
            self.currentAdState = MMAdState.AD_ERROR
            voAdDelegate?.notifyVOAdWith(eventName: .AdError, adInfo: voAdInfo)

        default:
            print("Other events")
        }
    }

    public func notifyAdEventWith(state: MMAdState, vastAd: VastAd?, vastTracker: VastTracker, andAdIndex index: Int) {
        if let adInstance = vastAd {
            let adInfo = self.fetchAndReportAdInfo(forAd: adInstance, adTracker: vastTracker, adIndex: index)
            self.mmssaiDelegate?.notifyAdEventsWith(eventName: state, andAdInfo: adInfo)
        }

        switch state {
        case .AD_REQUEST:
            self.mmSmartStreaming.report(MMAdState.AD_REQUEST)

        case .AD_IMPRESSION:
            self.isAdBuffering = false
            self.isAdStreaming = true
            self.mmSmartStreaming.reportAdPlaybackTime(0)
            self.mmSmartStreaming.report(MMAdState.AD_IMPRESSION)
            self.currentAdState = MMAdState.AD_IMPRESSION

        case .AD_STARTED:
            self.isAdBuffering = false
            self.isAdStreaming = true
            self.mmSmartStreaming.reportAdPlaybackTime(0)
            if (isAdBuffering) {
                self.isAdBuffering = false
            }
            self.mmSmartStreaming.report(MMAdState.AD_STARTED)
            self.currentAdState = MMAdState.AD_STARTED

        case .AD_CLICKED:
            self.isAdStreaming = false
            self.mmSmartStreaming.report(MMAdState.AD_CLICKED)
            self.currentAdState = MMAdState.AD_CLICKED

        case .AD_COMPLETED:
            self.isAdStreaming = false
            self.mmSmartStreaming.reportAdPlaybackTime(self.adDuration * 1000)
            self.mmSmartStreaming.report(MMAdState.AD_COMPLETED)
            self.currentAdState = MMAdState.AD_COMPLETED
            if (self.isPostRollAd) {
            }


        case .AD_ENDED:
            self.isAdStreaming = false
            self.mmSmartStreaming.report(MMAdState.AD_COMPLETED)
            self.currentAdState = MMAdState.AD_ENDED

        case .AD_PAUSED:
            self.isAdStreaming = true
            //self.mmSmartStreaming.reportAdPlaybackTime(Int(self.adInstance!.playheadTime()) * 1000)
            self.mmSmartStreaming.report(MMAdState.AD_PAUSED)
            self.currentAdState = MMAdState.AD_PAUSED

        case .AD_RESUMED:
            self.isAdStreaming = true
            self.mmSmartStreaming.report(MMAdState.AD_RESUMED)
            self.currentAdState = MMAdState.AD_RESUMED

        case .AD_SKIPPED:
            self.isAdStreaming = false
            self.mmSmartStreaming.report(MMAdState.AD_SKIPPED)
            self.currentAdState = MMAdState.AD_SKIPPED

        case .AD_FIRST_QUARTILE:
            self.isAdStreaming = true
            self.mmSmartStreaming.report(MMAdState.AD_FIRST_QUARTILE)
            self.currentAdState = MMAdState.AD_FIRST_QUARTILE

        case .AD_MIDPOINT:
            self.isAdStreaming = true
            self.mmSmartStreaming.report(MMAdState.AD_MIDPOINT)
            self.currentAdState = MMAdState.AD_MIDPOINT

        case .AD_THIRD_QUARTILE:
            self.isAdStreaming = true
            self.mmSmartStreaming.report(MMAdState.AD_THIRD_QUARTILE)
            self.currentAdState = MMAdState.AD_THIRD_QUARTILE

        case .AD_ERROR:
            self.mmSmartStreaming.report(MMAdState.AD_ERROR)
            self.currentAdState = MMAdState.AD_ERROR

        default:
            print("Other events")
        }
    }
}

public class VOPlayerAlternateInfo {
    

    var trackIndex: Int?
    var bitrate: Int?
    var width: Int?
    var height: Int?
    var codecIdentifier: String?
    
    public init(tIndex: Int?, bitRate: Int?, width: Int?, height: Int?, codecIdentifier: String?) {
        self.trackIndex = tIndex
        self.bitrate = bitRate
        self.width = width
        self.height = height
        self.codecIdentifier = codecIdentifier
    }
}
